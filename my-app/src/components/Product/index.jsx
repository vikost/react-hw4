import Button from "../Button";
import PropTypes from "prop-types";
import "./product.scss";
import { useDispatch, useSelector } from "react-redux";
import {
  addToCartModalContent,
  removeFromCartModalContent,
  removeFromFavoriteModalContent,
  toggleModalStatus,
} from "../../redux/actions/modal";
import { toggleLikedItem } from "../../redux/actions/products";

export default function Product(props) {
  const dispatch = useDispatch();

  const addedItems = useSelector((state) => state.products.addedToBasketItems);
  const likedItems = useSelector(
    (state) => state.products.addedToFavoritesItems
  );

  return (
    <li
      className="position-relative card me-3 mb-3"
      style={{ maxWidth: 250 + "px" }}
    >
      <div className="card-body">
        <img src={props.imageUrl} alt="" width={"230"} />
        <h5 className="card-title">{props.title}</h5>
        <p className="card-text">Vendor code: {props.code}</p>
        <p className="card-text">Color: {props.color}</p>
        <p className="card-text">Price: {props.price}</p>
        <Button
          btnStyle={"btn-dark"}
          text={props.buttonText}
          onButtonClick={() => {
            switch (props.type) {
              case "ALL_PRODUCTS":
                dispatch(addToCartModalContent(dispatch, props.code));
                break;

              case "FAV_PRODUCTS":
                dispatch(removeFromFavoriteModalContent(dispatch, props.code));
                break;

              case "BASKET_PRODUCTS":
                dispatch(removeFromCartModalContent(dispatch, props.code));
                break;
              default:
                break;
            }
            dispatch(toggleModalStatus());
          }}
          isDisabled={
            (addedItems.find((itemId) => itemId === props.code) &&
            props.type === "ALL_PRODUCTS")
              ? true
              : false
          }
        />
        {props.favIcon ? (
          <img
            src={
              likedItems.find((itemId) => itemId === props.code)
                ? "/images/favorite-active.png"
                : "/images/favorite.png"
            }
            className="favorite-icon"
            width={25}
            height={25}
            alt=""
            onClick={() => {
              dispatch(toggleLikedItem(props.code));
            }}
          />
        ) : null}
      </div>
    </li>
  );
}

Product.propTypes = {
  title: PropTypes.string,
  prise: PropTypes.string,
  imageUrl: PropTypes.string,
  color: PropTypes.string,
  code: PropTypes.string,
  favIcon: PropTypes.bool,
  type: PropTypes.string,
};

Product.defaultProps = {
  favIcon: true,
  type: "ALL_PRODUCTS",
};
