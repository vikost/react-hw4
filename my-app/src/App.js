import Header from "./components/Header";
import Modal from "./components/Modal";
import { Routes, Route } from "react-router-dom";
import { Home, Cart } from "./pages";
import { Favorite } from "./pages/favorite";
import { useDispatch, useSelector } from "react-redux";
import { toggleModalStatus } from "./redux/actions/modal";
import { useEffect } from "react";
import {fetchProducts} from "./redux/asyncActions/products";

function App() {
  const dispatch = useDispatch();
  const {
    modalContent: { modalTitle, modalText, modalActions, modalCloseBtn },
    modalIsOpen,
  } = useSelector((state) => state.modal);

  const {addedToBasketItems, addedToFavoritesItems } = useSelector(
    (state) => state.products
  );

  useEffect(() => {
    dispatch(fetchProducts());
  }, [dispatch]);
  return (
    <>
      <Header
        likedItems={addedToFavoritesItems}
        addedItems={addedToBasketItems}
      />
      <Routes>
        <Route index path="/" element={<Home />} />
        <Route path="/cart" element={<Cart />} />
        <Route path="/favorite" element={<Favorite />} />
      </Routes>
      <Modal
        title={modalTitle}
        text={modalText}
        closeButton={modalCloseBtn}
        actions={modalActions}
        modalIsOpen={modalIsOpen}
        closeModal={() => {
          dispatch(toggleModalStatus());
        }}
      />
    </>
  );
}

export default App;
