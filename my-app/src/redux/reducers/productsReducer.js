import { PRODUCTS } from "../types";

const initialState = {
  allProducts: [],
  addedToBasketItems: JSON.parse(localStorage.getItem("addedItems")),
  addedToFavoritesItems: JSON.parse(localStorage.getItem("favoriteItems")),
};

export function productsReducer(state = initialState, { type, payload }) {
  switch (type) {
    case PRODUCTS.GET_ALL_PRODUCTS:
      return { ...state, allProducts: payload };
    case PRODUCTS.TOGGLE_ADDED_ITEM:
      return { ...state, addedToBasketItems: payload };
    case PRODUCTS.TOGGLE_LIKED_ITEM:
      return { ...state, addedToFavoritesItems: payload };
    default:
      return state;
  }
}
