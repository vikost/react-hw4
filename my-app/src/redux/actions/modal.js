import { useDispatch } from "react-redux";
import Button from "../../components/Button";
import { MODAL } from "../types";
import { toggleAddedItem, toggleLikedItem } from "./products";

export function toggleModalStatus() {
  return { type: MODAL.TOGGLE_MODAL };
}
export function addToCartModalContent(dispatch, productId) {
  return {
    type: MODAL.SET_CONTENT,
    payload: {
      modalTitle: "Do you want to add this item to your shopping cart?",
      modalText: "Click OK to continue",
      modalActions: [
        <Button
          key={1}
          btnStyle={"btn-success"}
          text={"Ok"}
          onButtonClick={() => {
            dispatch(toggleAddedItem(productId))
            dispatch(toggleModalStatus())
          }}
        />,
        <Button
          key={2}
          btnStyle={"btn-outline-danger"}
          text={"Cancel"}
          onButtonClick={() => {
            dispatch(toggleModalStatus())
          }}
        />,
      ],
    },
  };
}

export function removeFromCartModalContent(dispatch, productId) {
  return {
    type: MODAL.SET_CONTENT,
    payload: {
      modalTitle: "Do you want to remove this item from your shopping cart?",
      modalText: "Click OK to continue",
      modalActions: [
        <Button
          key={1}
          btnStyle={"btn-success"}
          text={"Ok"}
          onButtonClick={() => {
            dispatch(toggleAddedItem(productId))
            dispatch(toggleModalStatus())
          }}
        />,
        <Button
          key={2}
          btnStyle={"btn-outline-danger"}
          text={"Cancel"}
          onButtonClick={() => {
            dispatch(toggleModalStatus())
          }}
        />,
      ],
    },
  };
}

export function removeFromFavoriteModalContent(dispatch, productId) {
  return {
    type: MODAL.SET_CONTENT,
    payload: {
      modalTitle: "Do you want to remove this item from your favorite list?",
      modalText: "Click OK to continue",
      modalActions: [
        <Button
          key={1}
          btnStyle={"btn-success"}
          text={"Ok"}
          onButtonClick={() => {
            dispatch(toggleLikedItem(productId))
            dispatch(toggleModalStatus())
          }}
        />,
        <Button
          key={2}
          btnStyle={"btn-outline-danger"}
          text={"Cancel"}
          onButtonClick={() => {
            dispatch(toggleModalStatus())
          }}
        />,
      ],
    },
  };
}