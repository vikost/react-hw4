import { PRODUCTS } from "../types";

export function getProducts(products) {
  return {
    type: PRODUCTS.GET_ALL_PRODUCTS,
    payload: products,
  };
}

export function toggleAddedItem(productId) {
  const updatedItems = JSON.parse(localStorage.getItem("addedItems"));
  const itemIndex = updatedItems.findIndex((item) => item === productId);

  if (itemIndex === -1) {
    updatedItems.push(productId);
  } else {
    updatedItems.splice(itemIndex, 1);
  }
  localStorage.setItem("addedItems", JSON.stringify(updatedItems));
  return {
    type: PRODUCTS.TOGGLE_ADDED_ITEM,
    payload: updatedItems,
  };
}

export function toggleLikedItem(productId) {
  const updatedItems = JSON.parse(localStorage.getItem("favoriteItems"));
  const itemIndex = updatedItems.findIndex((item) => item === productId);
  if (itemIndex === -1) {
    updatedItems.push(productId);
  } else {
    updatedItems.splice(itemIndex, 1);
  }
  localStorage.setItem("favoriteItems", JSON.stringify(updatedItems));
  return {
    type: PRODUCTS.TOGGLE_LIKED_ITEM,
    payload: updatedItems,
  };
}
