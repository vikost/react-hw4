import { ProductListTypes } from "../components/Product";
import ProductList from "../components/ProductList";
import { useSelector } from "react-redux";

export function Favorite() {

  const products = useSelector(state=>state.products.allProducts);
  const likedItems = useSelector(state=>state.products.addedToFavoritesItems);
  const likedProducts = products
    ? products.filter((product) => {
        if (likedItems.some((itemId) => itemId === product.code)) {
          return product;
        }
      })
    : null;
    
  return (
    <>
      <div className="container-lg">
        <h1>Favorite</h1>
      </div>
      <ProductList
        products={likedProducts}
        buttonText="Remove from favorite"
        type={"FAV_PRODUCTS"}
        favIcon={false}
      />
    </>
  );
}
