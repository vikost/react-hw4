import { ProductListTypes } from "../components/Product";
import ProductList from "../components/ProductList";
import { useSelector } from "react-redux";

export function Cart(props) {
  const products = useSelector(state=>state.products.allProducts);
  const addedItems = useSelector(state=>state.products.addedToBasketItems);

  const addedProducts = products
    ? products.filter((product) => {
        if (addedItems.some((itemId) => itemId === product.code)) {
          return product;
        }
      })
    : [];
    
  return (
    <>
      <div className="container-lg">
        <h1>Cart</h1>
      </div>
      <ProductList
        products={addedProducts}
        buttonText="Remove from shopping cart"
        type={"BASKET_PRODUCTS"}
        favIcon={false}
      />
    </>
  );
}

